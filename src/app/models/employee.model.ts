import { Country } from './country.model';
import { IdType } from './id-type.model';
import { Area } from './area.model';
export class Employee {

    constructor(
        public id?: number,
        public firstName?: string,
        public otherName?: string,
        public firstSurname?: string,
        public secondSurname?: string,
        public email?: string,
        public identification?: string,
        public startDate?: string,
        public creationDate?: string,
        public enabled?: boolean,

        public country?: Country,
        public idType?: IdType,
        public area?: Area,
    ) { }
}
