export class Area {

    constructor(
        public id?: number,
        public value?: string,
        public description?: string,
    ) { }
}
