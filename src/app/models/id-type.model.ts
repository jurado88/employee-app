export class IdType {

    constructor(
        public id?: number,
        public value?: string,
        public description?: string,
    ) { }
}
