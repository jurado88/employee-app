import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IdType } from '../models/id-type.model';

@Injectable({
  providedIn: 'root'
})
export class IdTypeService {

  private url = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  getIdsType(): Observable<IdType> {
    return this.httpClient.get(`${this.url}/idType/all`);
  }
}
