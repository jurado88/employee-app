import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Area } from '../models/area.model';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  private url = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  getAreas(): Observable<Area> {
    return this.httpClient.get(`${this.url}/area/all`);
  }
}
