import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private url = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  getData(page, pagesize, criterion) {
    let params = new HttpParams();
    params = params.append('page', page);
    params = params.append('size', pagesize);
    params = params.append('criterion', criterion);
    return this.httpClient.get(`${this.url}/employe/allPageable`, {params});
  }

  saveEmployee(employee: Employee) {
    return this.httpClient.post(`${this.url}/employe/add`, employee);
  }

  deleteEmployee(employee: Employee){
    return this.httpClient.request('delete', `${this.url}/employe/delete` , { body:  employee });
  }

  updateEmployee(employee: Employee){
    return this.httpClient.put(`${this.url}/employe/update`, employee);
  }

  getEmployeeByIdentification(identification: string){
    return this.httpClient.get(`${this.url}/employe/identification?id=${identification}`);
  }
}
