import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../models/country.model';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private url = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  getCountries(): Observable<Country> {
    return this.httpClient.get(`${this.url}/country/all`);
  }
}
