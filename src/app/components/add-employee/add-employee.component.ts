import { Component, EventEmitter, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Employee } from '../../models/employee.model';
import { EmployeeService } from '../../services/employee.service';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { AreaService } from '../../services/area.service';
import { CountryService } from '../../services/country.service';
import { IdTypeService } from '../../services/id-type.service';
import { Area } from '../../models/area.model';
import { Country } from '../../models/country.model';
import { IdType } from '../../models/id-type.model';
import { DatePipe } from '@angular/common';
import { CustomIdentificationValidator } from '../validators/custom-identification.validator';

export const YEAR_MODE_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD',
  },
  display: {
    dateInput: 'YYYY/MM/DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-CO' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: YEAR_MODE_FORMATS },
  ],
})
export class AddEmployeeComponent implements OnInit {

  public employeeForm: FormGroup;
  employee: Employee = {};
  loading = false;
  durationInSeconds = 5;
  onAdd = new EventEmitter<Employee>();
  areas: Area[] = [];
  area: Area;
  countries: Country[] = [];
  country: Country;
  idsType: IdType[] = [];
  idType: IdType;

  minDate: Date;
  maxDate: Date;
  date: Date;

  constructor(
    public dialogRef: MatDialogRef<AddEmployeeComponent>,
    public employeeService: EmployeeService,
    public areaService: AreaService,
    public countryService: CountryService,
    public idTypeService: IdTypeService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private datePipe: DatePipe,
    private customIdentificationValidartor: CustomIdentificationValidator,
    @Inject(MAT_DIALOG_DATA) public employeeOld: Employee
  ) {
    if (employeeOld){
      this.employee = {...employeeOld};
      this.idType = employeeOld.idType;
      this.country = employeeOld.country;
      this.area = employeeOld.area;
      this.date = new Date(employeeOld.startDate);
    }
    this.minDate = new Date();
    this.maxDate = new Date();
    this.minDate.setMonth(this.minDate.getMonth() - 1)
  }

  ngOnInit(): void {
    this.loading = false; 
    this.employeeForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(20)]],
      otherName: ['', [, Validators.maxLength(50)]],
      firstSurname: ['', [Validators.required, Validators.maxLength(20)]],
      secondSurname: ['', [Validators.required, Validators.maxLength(20)]],
      area: [this.employee.area, [Validators.required]],
      country: [this.country, [Validators.required]],
      idType: [this.employee.idType, [Validators.required]],
      identification: ['', [Validators.required, Validators.maxLength(20)],
        [this.customIdentificationValidartor.existingIdentificationValidator(this.employee.id)]],
      startDate: ['', [Validators.required]],
    });
    this.areaService.getAreas().subscribe( areas => {
      this.areas = areas as Area[];
    });
    this.countryService.getCountries().subscribe( coutries => {
      this.countries = coutries as Country[];
    });
    this.idTypeService.getIdsType().subscribe( idsType => {
      this.idsType = idsType as IdType[];
    });
  }


  registerEmployee(): void {
    console.log('resgiter employee', this.employee);
    if ( this.employeeForm.valid ){
      this.loading = true;
      this.employee.area = this.area;
      this.employee.country = this.country;
      this.employee.idType = this.idType;
      this.employee.startDate = this.datePipe.transform(this.date, 'yyyy/MM/dd');
      if( this.employeeOld ){
        this.employeeService.updateEmployee(this.employee).subscribe( (resp: Employee) => {
          this.loading = false;
          this.onAdd.emit(resp);
          this.dialogRef.close();
          this.snackBar.open(`Empleado actualizado exitosamente`, '', {
            duration: this.durationInSeconds * 1000,
            verticalPosition: 'top',
          });
          console.log('EMPLOYEE UPDATE', resp);
        });
      }else {
        this.employeeService.saveEmployee(this.employee).subscribe( (resp: Employee) => {
          this.loading = false;
          this.onAdd.emit(resp);
          this.dialogRef.close();
          this.snackBar.open(`Empleado creado exitosamente, correo: ${resp.email}`, '', {
            duration: this.durationInSeconds * 1000,
            verticalPosition: 'top',
          });
          console.log('EMPLOYEE CREATE', resp);
        });
      }
    }else {
      console.log('FormInvalid');
      this.employeeForm.markAllAsTouched();
    }
  }

  close(): void{
    this.dialogRef.close();
  }

  compareCategoryObjects(object1: any, object2: any): any {
    return object1 && object2 && object1.id == object2.id;
  }

}
