import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, debounceTime, take, switchMap } from 'rxjs/operators';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee.model';

function isEmptyInputValue(value: any): boolean {
  return value === null || value === '';
}

@Injectable({
  providedIn: 'root'
})
export class CustomIdentificationValidator {
  constructor(private employeeService: EmployeeService) {}

  existingIdentificationValidator(idEmployee: number): AsyncValidatorFn {
    return (
      control: AbstractControl
    ):
      | Observable<ValidationErrors | null> => {
      if (isEmptyInputValue(control.value)) {
        return of(null);
      } else {
        return control.valueChanges.pipe(
          debounceTime(500),
          take(1),
          switchMap(_ =>{
            return this.employeeService
              .getEmployeeByIdentification(control.value)
              .pipe(
                map((employee: Employee) =>{
                  if( idEmployee == null ){
                    return employee ? { existingIdentification: { value: control.value } } : null;
                  }else {
                    if( employee && employee.id !== idEmployee ){
                      return employee ? { existingIdentification: { value: control.value } } : null;
                    }
                  }
                }
                )
              );
            }
          )
        );
      }
    };
  }
}
