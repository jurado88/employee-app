import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Employee } from '../../models/employee.model';
import { EmployeeService } from '../../services/employee.service';
import { MatDialog } from '@angular/material/dialog';
import { AddEmployeeComponent } from '../add-employee/add-employee.component';
import { DeleteEmployeeComponent } from '../delete-employee/delete-employee.component';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['firstName', 'otherName', 'firstSurname', 'secondSurname', 'email', 'identification', 'idType', 'area', 'country', 'creationDate', 'action'];
  dataSource;
  ELEMENT_DATA: Employee[] = [];
  criterio = '';

  format = 'dd/MM/yyyy';
  locale = 'es-CO';

  length;
  page = 0;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 15];

  toolTipSearch = 'Búsquedas posibles\n por cualquier campo';

  constructor(
    private readonly employeeService: EmployeeService,
    public dialog: MatDialog
  )
  {
  }

  ngOnInit(): void {
    this.getData( 0, this.pageSize, '');
  }

  applyFilter() {
    let criterioAux = this.criterio;
    if (criterioAux && criterioAux.includes('/')) {
      let fechaArr = criterioAux.split('/');
      fechaArr = fechaArr.reverse();
      criterioAux = fechaArr.join('-');
    }
    this.criterio = criterioAux;
    this.getData(0, this.pageSize, this.criterio);
  }

  getData(page, pagesize, criterion) {
    this.ELEMENT_DATA = [];
    this.employeeService.getData(page, pagesize, criterion).subscribe( ( resp: any ) => {
      this.length = resp.totalItems;
      this.ELEMENT_DATA = [];
      resp.employees.forEach( (element: any) => {
        this.ELEMENT_DATA.push(element);
      });
      this.dataSource =  new MatTableDataSource(this.ELEMENT_DATA);
      this.dataSource.sort = this.sort;
    });
  }

  changePage(event) {
    this.page = event.pageIndex;
    this.getData(event.pageIndex, event.pageSize, this.criterio);
  }

  addEmployee(): void{
    // console.log('SCOPE EMAWEB', EmaWeb.usuarioSesion);
    const dialogRef = this.dialog.open(AddEmployeeComponent, {
      width: '350px'
    });
    const sub = dialogRef.componentInstance.onAdd.subscribe((item: Employee) => {
      this.getData(0, this.pageSize, this.criterio);
    });
    dialogRef.afterClosed().subscribe(() => {
      // unsubscribe onAdd
    });
  }

  updateEmployee(employee: Employee): void{
    const dialogRef = this.dialog.open(AddEmployeeComponent, {
      width: '350px',
      data: employee
    });
    const sub = dialogRef.componentInstance.onAdd.subscribe((item: Employee) => {
      this.getData(this.page, this.pageSize, this.criterio);
    });
    dialogRef.afterClosed().subscribe(() => {
      // unsubscribe onAdd
    });
  }

  deleteEmployee(employee: Employee): void {
    this.dialog
      .open(DeleteEmployeeComponent, {
        data: `¿Esta seguro de eliminar el empleado seleccionado?`
      })
      .afterClosed()
      .subscribe((confirmado: boolean) => {
        if (confirmado) {
          this.employeeService.deleteEmployee(employee).subscribe( resp => {
            this.getData(this.page, this.pageSize, this.criterio);
          });
        }
    });
  }

}
